.. iosprivacydata documentation master file, created by
   sphinx-quickstart on Mon Mar 29 10:24:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to iOSprivacyData's documentation!
==========================================

A CLI tool to show what kind of data an iOS app collects from the users.

* The data is scraped from official Apple App Store web-page for the App.
* The website says the information has not been verified by Apple but it should give an idea about the App.

==================
Table of Contents
==================

.. toctree::
   :maxdepth: 2

   quick_start_guide
   modules
   privacy_data_type

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Notes
-----
.. note:: Logo made from Icons by `Freepik <https://www.freepik.com>`_ from `<https://www.flaticon.com>`_
