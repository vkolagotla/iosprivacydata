Privacy Data Types
==================

This page gives a general idea for what kind of data
might come under the mentioned data in the results. Some apps might not
collect all the data that is mentioned under a particular data type,
in most of the cases it just depends on the type of the app.

**The collected may be used for the following PURPOSES:**

* Third-Party Advertising
* Developer’s Advertising or Marketing
* Analytics
* Product Personalization
* App Functionality
* Other Purposes

**The collected data can be classified into 3 CLASSES:**

Data Used to Track You
----------------------
This data may be used to track you across apps and websites owned by other companies.

Data Linked to You
----------------------
This data, which may be collected and linked to your identity.

Data Not Linked to You
--------------------------
This data, which may be collected but is not linked to your identity.

**Following is the data that is collected by the app, which they can further put into any
of the above mentioned purposes:**

Contact Info
----------------
* Physical Address
    * Postal address that an user probably provides for whatever reason
* Email Address
* Name
* Phone Number
* Other User Contact Info

Contacts
------------
* Contacts
    * Contacts from your mobile. Some apps might get this info without concent.

Identifiers
---------------
* User ID
    * probably given by the app to each user
* Device ID
    * Could be your device ID(like IMEI or MAC address🤔)
    * This can be used track your activity if you are using multiple accounts from same device.

Location
------------
* Coarse Location
    * Usually Country or region based on IP address, probably collected automatically.
* Precise Location
    * Exact GPS location of the device.

Browsing History
--------------------
* Browsing History
    * Not sure if they are actually referring to browser history, but if that's the case then that's messed up.
  
Usage Data
--------------
* Product Interaction
    * In-App product interaction data
* Advertising Data
    * Interaction with In-App advertisements
* Other Usage Data

Purchases
-------------
* Purchase History
    * In-App purchase data

User Content
----------------
* Photos or Videos
    * Shared with in the app.
* Emails or Text Messages
    * This can be creepy if they are referring to all Emails and Messages.
    * Another reason to use End-to-End Encrypted Emails.
* Audio Data
    * Probably generated from using their app for calls or voice messages.
* Gameplay Content
* Customer Support
* Other User Content

Sensitive Info
------------------
* Sensitive Info
    * Not sure what this means?🤔

Search History
------------------
* Search History
    * In-App search history

Diagnostics
---------------
* Crash Data
* Performance Data
* Other Diagnostic Data

Financial Info
------------------
* Payment Info
    * Probably provided by the user/ or maybe some tags they use in iOS
* Other Financial Info

Health & Fitness
--------------------
* Health and Fitness
    * Data recorded in the Health app

Other Data
--------------
* Other Data Types

An app collects the mentioned data mostly with the concent of the users. An app might not
collect all the sub types of data mentioned above. For example when an app collects
``Contact Info``, it might not collect all types of the data that comes under ``Contact Info``.
Some might just collect phone number and email address. The user usually gives this data
for the app to work. It's highly unlikely for an app to acquire this data with out the user
knowledge unless the app is some kind of spyware/trojan. It's also same for ``Location data``
where not all apps collects ``Precise Location``, some just collects ``Coarse Location`` which
is usually the country/region.

There are how ever other types of data like Browsing History(creepy right!😑), Sensitive Data or 
Health and Fitness data that certain apps collects with out the knowledge of the
user.

If an app can get hands on all this data from so many user, it's definitely possible
to manipulate someones behavior with showing Ads/posts/videos which falls into that behavior
pattern which isn't good most of the time for most of the people. That to me is pretty messed
up on a fundamental level.
