iosprivacydata.iosprivacydata module
------------------------------------

.. automodule:: iosprivacydata.iosprivacydata
   :members:
   :undoc-members:
   :show-inheritance:


.. automodule:: iosprivacydata
   :members:
   :undoc-members:
   :show-inheritance:
