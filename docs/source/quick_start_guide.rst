Quick Start Guide
=================

Installation
------------
``iOSprivacyData`` requires Python 3.6+

The current development version is available on `Gitlab
<https://gitlab.com/vkolagotla/iosprivacydata>`__. Use :command:`git` and
:command:`pip` to install it:

Installing ``iOSprivacyData`` is super easy, just clone it from gitlab and install the latest stable version by doing:

.. code-block:: bash

   git clone https://gitlab.com/vkolagotla/iosprivacydata.git
   cd iosprivacydata
   pip install .
   # or you can install it directly from git
   pip install git+https://gitlab.com/vkolagotla/iosprivacydata

Features
--------
- Shows what kind of data an app collects

Example Usage
-------------
Following code block shows how to use iOSprivacyData CLI tool. It needs only one argument
``-a`` for app name.

.. code-block:: bash

    iosprivacydata -a APPNAME

    # `-v` for version
    iosprivacydata -v
    iosprivacydata v0.1.3

    # `-h` for help/usage guide
    iosprivacydata -h
    usage: iosprivacydata [-h] [-v] [-a APP_NAME]

    * Shows what kind of data an app collects from you(Only iOS)🕵
    * Use Quotes for more than one word searches🤷
    * Try using app related words in search incase you do not see any result😎
    * Try not to make too many frequent requests

    optional arguments:
      -h, --help            show this help message and exit
      -v, --version         show programs version number and exit
      -a APP_NAME, --app_name APP_NAME
                            app name to search    

Example Results
---------------
**Keybase iOS App**

.. code-block:: bash

    iosprivacydata -a keybase
    What kind of data does Keybase collects from it's users?🤔

    Data Not Collected
    The developer does not collect any data from this app.

    Check the original data here: https://apps.apple.com/de/app/keybase-crypto-for-everyone/id1044461770


**TikTok iOS App**

.. code-block:: bash

    iosprivacydata -a tiktok
    What kind of data does Tiktok collects from it's users?🤔

    Data Used to Track You
    The following data may be used to track you across apps and websites owned by other companies:
    ---------------
    Contact Info
    Identifiers
    
    Data Linked to You
    The following data may be collected and linked to your identity:
    ---------------
    Purchases
    Financial Info
    Location
    Contact Info
    Contacts
    User Content
    Search History
    Browsing History
    Identifiers
    Usage Data
    Diagnostics

    Data Not Linked to You
    The following data may be collected but it is not linked to your identity:
    ---------------
    Usage Data

    Check the original data here: https://apps.apple.com/ug/app/tiktok/id835599320

Tips
----
.. tip:: Try using app related words in search incase you do not see any/expected result.

Notes
-----
.. note:: The App Store does not mention anything about the metadata apps collect from their users. This could be for internal use like to keep the app working properly but this allows someone get their hands on the data.

.. note:: The data is scraped from official Apple App Store web-page for the App.

.. note:: The apple website says the information has not been verified by Apple but it should give an idea about the App.

Warnings
--------
.. warning:: Don't do too many frequent requests since you would be overloading the requests.

.. warning:: I created this for my personal use, so **DO NOT** use it for any commercial purpose.
