<p align="center"><img src="assets/logo.png" width="140" height="160">
<h2 align="center">iOSprivacyData</h2>

A CLI tool to show what kind of data an iOS app collects from the users. [Read the Docs](https://vkolagotla.gitlab.io/iosprivacydata/).


* The data is scraped from official Apple App Store web-page for the App.
* The website says the information has not been verified by Apple but it should give an idea about the App.

## How to install?

Just clone the repo and install it as any other python package.

```bash
pip install .
```

## How to use?

It's pretty straight forward. The program needs one argument `-a` for the app name. The default language for search search is English.

```bash
 iosprivacydata -a APPNAME
```

`-h` for help/usage guide

```bash
iosprivacydata -h
usage: iosprivacydata [-h] [-v] [-a APP_NAME]

* Shows what kind of data an app collects from you(Only iOS)🕵
* Use Quotes for more than one word searches🤷
* Try using app related words in search incase you do not see any result😎
* Try not to make too many frequent requests

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show programs version number and exit
  -a APP_NAME, --app_name APP_NAME
                        app name to search
```

## Example result

**`Keybase` iOS App**

```bash
iosprivacydata -a keybase
What kind of data does Keybase collects from its users?🤔

Data Not Collected
The developer does not collect any data from this app.
<!-- -->

Check the original data here: https://apps.apple.com/de/app/keybase-crypto-for-everyone/id1044461770
```

**`TikTok` iOS App**

```bash
iosprivacydata -a tiktok
What kind of data does Tiktok collects from its users?🤔

Data Used to Track You
The following data may be used to track you across apps and websites owned by other companies:
---------------
Contact Info
Identifiers
 
Data Linked to You
The following data may be collected and linked to your identity:
---------------
Purchases
Financial Info
Location
Contact Info
Contacts
User Content
Search History
Browsing History
Identifiers
Usage Data
Diagnostics
 
Data Not Linked to You
The following data may be collected but it is not linked to your identity:
---------------
Usage Data

Check the original data here: https://apps.apple.com/ug/app/tiktok/id835599320
```

## LICENSE

I created this for my personal use, so **DO NOT** use it for any commercial purposes.

## Credits

<div>Logo made from Icons by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

## TODO

- [x] Replace Google with DuckDuckGo
- [ ] Use Soup to cleanup and get the privacy data instead of `str.replace()`
- [ ] Give rating for the app? or at least classify them.
- [ ] Generate reports for multiple apps at a time?

## [CHANGELOG](/CHANGELOG)