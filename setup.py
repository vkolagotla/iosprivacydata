#!/usr/bin/env python3

__author__ = "Venkata Kolagotla"
__copyright__ = "Copyright 2021"
__credits__ = ["Venkata Kolagotla"]
__license__ = "None"
__version__ = "0.1.4"
__maintainer__ = "Venkata Kolagotla"
__email__ = "vkolagotla@pm.me"
__status__ = "Development"


from setuptools import setup, find_packages

with open("README.md", "r") as f:
    long_description = f.read()

with open("requirements.txt") as f:
    required = f.read().splitlines()

setup(
    name="iosprivacydata",
    version="",
    author="Venkata Kolagotla",
    author_email="vkolagotla@pm.me",
    description="A CLI tool to show what kind of data an iOS app collects from the users.",
    long_description=long_description,
    url="https://gitlab.com/vkolagotla/iosprivacydata.git",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=required,
    entry_points={
        "console_scripts": ["iosprivacydata = iosprivacydata.iosprivacydata:main"]
    },
)
