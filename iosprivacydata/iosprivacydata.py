#!/usr/bin/env python3

__author__ = "Venkata Kolagotla"
__copyright__ = "Copyright 2021"
__credits__ = ["Venkata Kolagotla"]
__license__ = "None"
__version__ = "0.1.4"
__maintainer__ = "Venkata Kolagotla"
__email__ = "vkolagotla@pm.me"
__status__ = "Development"


import re
import requests
import argparse
from typing import List

from termcolor import colored
from bs4 import BeautifulSoup
from ScrapeSearchEngine.ScrapeSearchEngine import Duckduckgo


PACKAGE_DESCRIPTION = """
* Shows what kind of data an app collects from you(Only iOS)🕵
* Use Quotes for more than one word searches🤷
* Try using app related words in search incase you do not see any result😎
* Try not to make too many frequent requests
"""


def parse_args():
    """Get command line arguments."""
    parser = argparse.ArgumentParser(
        prog="iosprivacydata",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=PACKAGE_DESCRIPTION,
    )
    parser.add_argument("-v", "--version", action="version", version="%(prog)s v")
    parser.add_argument("-a", "--app_name", type=str, help=r"app name to search")
    return parser.parse_args()


def get_appstore_url(app_name: str) -> List[str]:
    """Takes an app name as input and returns the top links from search.
    First one is usually the correct one.

    Parameters
    ----------
    app_name : str
        App name to get app store url

    Returns
    -------
    List(str)
        Links for top search results
    """
    search = app_name + " ios-app apple app-store link"
    userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"  # noqa
    link_list = Duckduckgo(search, userAgent)
    return link_list


def get_privacy_data(app_name: str, url: str) -> str:
    """Takes the webpage and create a response object and extracts the data.

    Parameters
    ----------
    app_name : str
        App name to get app store url
    url : str
        Correct app store url

    Returns
    -------
    str
        Privacy data collected by the given App
    """
    response = requests.get(url)

    # extract the source code of the page.
    data = response.text
    soup = BeautifulSoup(data, "lxml")

    app_privacy_data = soup.find_all("div", {"class": "app-privacy__card"})
    app_privacy_data = str(app_privacy_data)

    app_privacy_data = re.sub(r"<svg.+?>", "", app_privacy_data)
    app_privacy_data = re.sub(r"<path.+?>", "", app_privacy_data)
    app_privacy_data = app_privacy_data.replace("</path></svg>", "")
    app_privacy_data = re.sub(r"<li.+?>", "", app_privacy_data)
    app_privacy_data = app_privacy_data.replace("</li>", "")
    app_privacy_data = re.sub(r"<ul.+?>", "", app_privacy_data)
    app_privacy_data = app_privacy_data.replace("</ul>", "")
    app_privacy_data = re.sub(r"<h3.+?>", "", app_privacy_data)
    app_privacy_data = app_privacy_data.replace("</h3>", "")
    app_privacy_data = re.sub(r"<p.+?>", "", app_privacy_data)
    app_privacy_data = app_privacy_data.replace("</p>", "")

    app_privacy_data = app_privacy_data.replace("</div>", "")
    app_privacy_data = app_privacy_data.replace("<div>", "")
    app_privacy_data = app_privacy_data.replace('<span class="privacy-type__grid">', "")
    app_privacy_data = app_privacy_data.replace(
        '<span class="privacy-type__grid-icon">', ""
    )
    app_privacy_data = app_privacy_data.replace(
        '<span class="privacy-type__grid-content privacy-type__data-category-heading">',
        "",
    )
    app_privacy_data = app_privacy_data.replace("</span>", "")
    app_privacy_data = app_privacy_data.replace('<div class="privacy-type__icon">', "")
    app_privacy_data = app_privacy_data.replace('<div class="app-privacy__card">', "")
    app_privacy_data = app_privacy_data.replace("[", "")
    app_privacy_data = app_privacy_data.replace("]", "")
    app_privacy_data = app_privacy_data.replace("&amp;", "and")
    app_privacy_data = app_privacy_data.replace(":", ":\n---------------")
    app_privacy_data = re.sub(r"\n+", "\n", app_privacy_data).strip()

    # print app details and the data it collects, if any
    print(
        "What kind of data does {} collects from it's users?🤔\n".format(
            app_name.capitalize()
        )
    )
    print(app_privacy_data)
    return app_privacy_data


def main():
    """Find the data an app collects for iOS users.
    Takes the app name and returns the data collection details."""
    args = parse_args()
    try:
        urls = get_appstore_url(args.app_name)
        if urls.__len__() > 0:
            get_privacy_data(args.app_name, urls[0])
        else:
            print("Something went wrong somewhere...🤔")
            url_error = colored(
                "Looks like the app-page url is not fetched...Damn🙄",
                "red",
                attrs=["reverse", "blink"],
            )
            print(url_error)
            print("Please try again...🤦")
    except IndexError as e:
        print("Here is the Error message, this might help...")
        print(e)
    else:
        else_text = colored(
            "\nCheck the original data here:", "green", attrs=["reverse", "blink"]
        )
        print(else_text + " {}".format(urls[0]))


if __name__ == "__main__":
    main()
