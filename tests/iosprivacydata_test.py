#!/usr/bin/env python3

__author__ = "Venkata Kolagotla"
__copyright__ = "Copyright 2021"
__credits__ = ["Venkata Kolagotla"]
__license__ = "None"
__version__ = "0.1.4"
__maintainer__ = "Venkata Kolagotla"
__email__ = "vkolagotla@pm.me"
__status__ = "Development"

from iosprivacydata import get_appstore_url, get_privacy_data  # , parse_args, main


# def test_parser():
#     """Test argparser inputs"""
#     parser = parse_args()
#     assert parser.lang == str
#     assert parser.app_name == str


def test_get_appstore_url():
    """Test url fetch for a given app"""
    url_list = get_appstore_url("keybase")
    assert type(url_list) == list
    assert len(url_list) > 0
    assert type(url_list[0]) == str


def test_get_privacy_data():
    """Test privacy data fetch from the url"""
    privacy_data = get_privacy_data(
        "keybase",
        "https://apps.apple.com/us/app/keybase-crypto-for-everyone/id1044461770#?platform=iphone",
    )

    assert type(privacy_data) == str
    assert len(privacy_data) > 0
