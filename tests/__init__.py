#!/usr/bin/env python3

__author__ = "Venkata Kolagotla"
__copyright__ = "Copyright 2021"
__credits__ = ["Venkata Kolagotla"]
__license__ = "None"
__version__ = "0.1.4"
__maintainer__ = "Venkata Kolagotla"
__email__ = "vkolagotla@pm.me"
__status__ = "Development"
